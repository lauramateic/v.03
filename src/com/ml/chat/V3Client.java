package com.ml.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Created by asus on 10/28/2015.
 */
public class V3Client extends Thread {

    public static void main(String[] args) throws Exception {
        V3Client v3c = new V3Client();
        v3c.start();
    }

    public void run() {

        try {
            System.out.println("Client ready , sending request...");
            Socket socket = new Socket("localhost", 2500);
            ;
            System.out.println("Connection completed , sending message");

            PrintStream out = new PrintStream(socket.getOutputStream());
            out.println("Hello server, its a test message");

            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line = br.readLine();
            System.out.println(line);


            BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
            String name = "";

            String msg = "";
            //msg=console.readLine()!=null
            System.out.print("username: ");
            name = console.readLine();
            out.println(" *** user: " + name + " entered the chat!");
            while (name != null) {

                System.out.print(name + " : ");
                if ((msg = console.readLine()) != null) {
                    out.println(msg);
                    if (msg.equals("exit")) {
                        out.println(" *** user " + name + " signed out!");
                        break;
                    }

                }


            }


            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
