package com.ml.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by asus on 10/28/2015.
 */
public class V3Server {

    private boolean listening = true;

    public static void main(String[] args) throws Exception {
        System.out.println("var 2 server");
        V3Server server = new V3Server();
        server.listen();
    }

    public void listen() throws IOException {

        ServerSocket server = new ServerSocket(2500);//the server is listening to at this port

        while (listening) {
            System.out.println("Server is ready , waiting for a connection request...");
            final Socket sock = server.accept();
            System.out.println("Request received , connection completed...");

            new Thread() {
                public void run() {
                    try {
                        BufferedReader br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                        String msg;
                        PrintStream out = new PrintStream(sock.getOutputStream());
                        out.println("Welcome!");
                        while ((msg = br.readLine()) != null) {
                            System.out.println("Received message " + msg);
                        }
                        sock.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        server.close();
    }
}
